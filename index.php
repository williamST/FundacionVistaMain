<?php
require_once("modelo/Conexion.php");
require_once("modelo/Doctor.php");
?> 

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Fundación Vista</title>
    <meta name="description" content="">
    <meta name="author" content="">
	<!--Especificaciones mobiles-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<!--Fuentes-->
	<link href='http://fonts.googleapis.com/css?family=Lato:400italic,100,300,700,800,400' rel='stylesheet' type='text/css'>
	<!--Iconos-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--CSS-->
	<link rel="stylesheet" href="css/bower.css" />
    <link rel="stylesheet" href="css/styles.css">
    <!--Icon-->
    <link rel="icon" type="image/png" href="img/logos/logo_login.png" />
    <link rel="SHORTCUT ICON" href="img/logos/logo_login.png" />
    <!--JS-->
    <script src="js/bower.js"></script>
    <script src="js/functions.js"></script>
    <script src="js/scripts.js"></script> 
</head>
<body>
     <!--Pre-Carga -->
    <header class="ip-header">
        <div>
            <img src="img/logos/logo_login.png">
            <h5 class="center weight-100 grey-text text-lighten-3"><b>FUNDACIÓN VISTA</b></h5>
            <p class="weight-300 center grey-text text-darken-0 uppercase">Please wait...</p>
    </header>
    <!--Start -->
    <script src="js/parallax.js"></script>
    <!-- Nav -->
    <!-- Navbar -->
    <ul id="dropdownSkills" class="dropdown-content">
        <li><a href="#photography" onclick="$('.photographySlider').moveToSlide(1)">Exámenes</a>
        </li>
        <li><a href="#filmmaking" onclick="$('.filmmakingSlider').moveToSlide(1)">Consultas</a>
        </li>
        <li><a href="#web" onclick="$('.webSlider').moveToSlide(1)">Cirugías</a>
        </li>
    </ul>   

    <div class="bg-header"></div>
    <div class="navbar-fixed">
        <nav>
            <div class="backgroundImage"></div>
            <div class="container">
                <div class="nav-wrapper">
                    <a class="brand-logo text-logo" href="#splash"><b>FUNDACI&Oacute;N VISTA</b>.com</a>
                    <a class="brand-logo icon-logo" href="#splash"><img src="img/logos/logo_login.png">
                    <ul id="nav-mobile" class="right side-nav">
                        <li class="hide-on-med-and-down"><a href="#work">CLINICA</a>
                        </li>
                        <li><a class="dropdown-button" data-activates="dropdownSkills" href="#what">SERVICIOS</a>
                        </li>
                        <li><a href="#who" id="link-about">STAFF</a>
                        </li>
                        <li><a href="#gallery">OPTICA</a>
                        </li>
                        <li><a href="#how">CONTACTENOS</a>
                        </li>
                    </ul>
                    <!--
                    #work = clinica // #what = servicios // #who = staff // #gallery = optica // #who = contactenos
                    -->
                    <!-- Include this line below -->
                    <a class="button-collapse" href="#" data-activates="nav-mobile"><i class="material-icons">reorder</i></a>
                    <!-- End -->
                </div>
            </div>
        </nav>
    </div>
    <!-- Splash -->
    <!-- Imagen fondo -->
    <div id="splash" class="img-holder-0 " data-image-xl="img/background_home_xl.jpg" data-image-l="img/background_home_l.jpg" data-image-m="img/background_home_m.jpg" data-image-s="img/background_home_s.jpg" data-image-touch="img/background_home_text.jpg" data-cover-ratio="1">
        <div class="container header-container hide-on-med-and-down" style="-webkit-transform: translateZ(1px)">
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <h2 class="text-center text-white header"><b>"Cuidamos tus ojos para que vivas mejor"</b></h2>
        </div>
    </div>
    <script>
        var touch;
        if ($('body').width() < 992 || Modernizr.touch) {
            touch = true;
        } else {
            touch = false;
        }
        //Parallax
        $('.img-holder-0').imageScroll({
            imageAttribute: 'image-' + windowSize($window.width()),
            touch: touch,
            holderMinHeight: 500,
            speed: 0.1
        });
    </script>
    <!-- About -->
    <!-- Work = clinica -->
    <div id="work" class="section-link">
        <!-- 1 item -->
        <div id="work" class="section-link white black-text ">
            <section class="hide-on-med-and-down">
                <div class="sectionSlider workSlider1 hideButtons">
                    <ul>
                        <li>
                            <div class="container">
                                <div class="row">
                                    <div class="col s12">
                                        <div class="row">
                                            <div class="col s10 m6 l4 offset-s1 grid">
                                                <figure class="effect-bubba">
                                                    <img src="img/clinica/conocenos.jpg" alt="img02" class="responsive-img" />
                                                    <figcaption>
                                                        <h4 class="weight-300 white-text"><b>¿QUI&Eacute;NES SOMOS?</b></h4>
                                                        <p class="grey-text text-darken-4"></p>                
                                                        <a href="javascript:$('.workSlider1').moveToLink(0)" class="no-smooth-scroll">View more</a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                            <!-- Fin 1 item -->
                                            <!-- 2 Item -->
                                            <div class="col s10 m6 l4 offset-s1 grid ">
                                                <figure class="effect-bubba">
                                                    <img src="img/mision/mision.jpg" alt="img02" class="responsive-img" />
                                                    <figcaption>
                                                        <h4 class="weight-300"><b>NUESTRA MISIÓN</b></h4>
                                                        <p></p>
                                                        <a href="javascript:$('.workSlider1').moveToLink(1)" class="no-smooth-scroll">View more</a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                            <!-- Fin 2 - Item -->
                                            <!--3 - Item -->
                                            <div class="col s10 m6 l4 offset-s1 grid ">
                                                <figure class="effect-bubba">
                                                    <img src="img/valores/valores.jpg" alt="img02" class="responsive-img" />
                                                    <figcaption>
                                                        <h4 class="weight-300"><b>NUESTROS VALORES</b></h4>
                                                        <p></p>
                                                        <a href="javascript:$('.workSlider1').moveToLink(2)" class="no-smooth-scroll">View more</a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                            <!-- Fin 3 - Item -->
                                            <!--4 - Item -->
                                            <div class="col s10 m6 l4 offset-s1 grid ">
                                                <figure class="effect-bubba">
                                                    <img src="img/convenios/convenios.jpg" alt="img02" class="responsive-img" />
                                                    <figcaption>
                                                        <h4 class="weight-300"><b>NUESTROS CONVENIOS</b></h4>
                                                        <p></p>
                                                        <a href="javascript:$('.workSlider1').moveToLink(3)" class="no-smooth-scroll">View more</a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                            <!-- Fin 4 - Item -->
                                            <!-- 5- Item -->
                                            <div class="col s10 m6 l4 offset-s1 grid ">
                                                <figure class="effect-bubba">
                                                    <img src="img/promociones/promociones.jpg" alt="img02" class="responsive-img" />
                                                    <figcaption>
                                                        <h5 class="weight-300"><b>NUESTRAS PROMOCIONES</b></h5>
                                                        <p></p>
                                                        <a href="javascript:$('.workSlider1').moveToLink(4)" class="no-smooth-scroll">View more</a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                            <!-- Fin 5 - Item -->
                                            <!-- 6- Item -->
                                            <div class="col s10 m6 l4 offset-s1 grid ">
                                                <figure class="effect-bubba">
                                                    <img src="img/campañas/campañas.jpg" alt="img02" class="responsive-img" />
                                                    <figcaption>
                                                        <h4 class="weight-300"><b>NUESTRAS CAMPAÑAS</b></h4>
                                                        <p></p>
                                                        <a href="javascript:$('.workSlider1').moveToLink(5)" class="no-smooth-scroll">View more</a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                            <!-- Fin 6 - Item -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="link">
                            <div class="container">
                                <div class="row">
                                    <div class="col s12 m10 offset-m1 l4 animatedBox animatedBox-left animatedBox-list">
                                        <h6 class="text-center"><b>FUNDACI&Oacute;N VISTA</b></h6>
                                        <hr>
                                        <h6 class="weight-600" style="text-align:justify">Somos una institución altamente especializada en la atención oftalmológica, contamos con una moderna infraestructura, equipos de última tecnología y procedimientos computarizados de ayuda al diagnóstico para restablecer tu salud.</h6>
                                        <h6 class="text-center"><b>HORARIOS</b></h6>
                                        <h6 class="weight-600" style="text-align:justify">Lun -Vie: 10:00 am - 19:00 pm <br> Sab:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10:00 am - 14:00 pm</h6>
                                        <hr>
                                        <center>  <a href="javascript:$('.sectionSlider').backToSlide()" class="btn no-smooth-scroll"><i class="material-icons">fast_rewind</i></a></center>
                                    </div>
                                    <div class="col s12 m9 l8 offset-m1 animatedBox animatedBox-right">
                                        <div class="fadein video-container">
                                            <img src="img/clinica/clinica-1.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/clinica/clinica-2.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/clinica/clinica-3.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/clinica/clinica-4.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/clinica/clinica-5.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/clinica/clinica-6.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/clinica/clinica-7.jpg" class="responsive-img z-depth-3" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="link">
                            <div class="container">
                                <div class="row">
                                    <div class="col s12 m10 offset-m1 l4 animatedBox animatedBox-left animatedBox-list">
                                        <h6 class="text-center"><b>NUESTRA MISIÓN</b></h6>
                                        <hr>
                                        <h6 class="weight-600" style="text-align:justify">Brindar a nuestros pacientes una atención de calidad con el respaldo de especialistas de excelente perfil profesional unido a un espíritu de superación constante.</h6>
                                        <hr>
                                        <center><a href="javascript:$('.sectionSlider').backToSlide()" class="btn no-smooth-scroll"><i class="material-icons">fast_rewind</i></a></center>
                                    </div>
                                    <div class="col s12 m9 l8 offset-m1 animatedBox animatedBox-right">
                                        <div class="fadein video-container">
                                            <img src="img/mision/mision-1.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/mision/mision-2.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/mision/mision-3.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/mision/mision-4.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/mision/mision-5.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/mision/mision-6.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/mision/mision-7.jpg" class="responsive-img z-depth-3" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="link">
                            <div class="container">
                                <div class="row">
                                    <div class="col s12 m10 offset-m1 l4 animatedBox animatedBox-left animatedBox-list">
                                        <h6 class="text-center"><b>NUESTROS VALORES</b></h6>
                                        <hr>
                                        <h6 class="weight-600" style="text-align:justify">Lealtad: Integridad, honestidad y respeto<br>Orientación al cliente: Actitud de servicio, flexibilidad, empatía y excelencia<br>Cumplimiento: Responsabilidad, compromiso y perseverancia<br>Cooperación: Trabajo en equipo, apoyo a los demás<br>Espíritu Emprendedor: Superación, decisión, iniciativa y creatividad.</h6>
                                        <hr>
                                        <center><a href="javascript:$('.sectionSlider').backToSlide()" class="btn no-smooth-scroll"><i class="material-icons">fast_rewind</i></a></center>
                                    </div>
                                    <div class="col s12 m9 l8 offset-m1 animatedBox animatedBox-right">
                                        <div class="fadein video-container">
                                            <img src="img/valores/valor-1.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/valores/valor-2.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/valores/valor-3.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/valores/valor-4.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/valores/valor-5.jpg" class="responsive-img z-depth-3" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="link">
                            <div class="container">
                                <div class="row">
                                    <div class="col s12 m10 offset-m1 l4 animatedBox animatedBox-left animatedBox-list">
                                        <h6 class="text-center"><b>NUESTROS CONVENIOS</b></h6>
                                        <hr>
                                        <h6 class="weight-600" style="text-align:center">Sección en mantenimiento.</h6>
                                        <hr>
                                        <center><a href="javascript:$('.sectionSlider').backToSlide()" class="btn no-smooth-scroll"><i class="material-icons">fast_rewind</i></a></center>
                                    </div>
                                    <div class="col s12 m9 l8 offset-m1 animatedBox animatedBox-right">
                                        <div class="fadein video-container">
                                            <img src="img/convenios/convenio-1.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/convenios/convenio-2.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/convenios/convenio-3.jpg" class="responsive-img z-depth-3" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="link">
                            <div class="container">
                                <div class="row">
                                    <div class="col s12 m10 offset-m1 l4 animatedBox animatedBox-left animatedBox-list">
                                        <h6 class="text-center"><b>NUESTRAS PROMOCIONES</b></h6>
                                        <hr>
                                        <h6 class="weight-600" style="text-align:center">Sección en mantenimiento.</h6>
                                        <hr>
                                        <center><a href="javascript:$('.sectionSlider').backToSlide()" class="btn no-smooth-scroll"><i class="material-icons">fast_rewind</i></a></center>
                                    </div>
                                    <div class="col s12 m9 l8 offset-m1 animatedBox animatedBox-right">
                                        <div class="fadein video-container">
                                            <img src="img/promociones/promocion-1.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/promociones/promocion-2.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/promociones/promocion-3.jpg" class="responsive-img z-depth-3" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="link">
                            <div class="container">
                                <div class="row">
                                    <div class="col s12 m10 offset-m1 l4 animatedBox animatedBox-left animatedBox-list">
                                        <h6 class="text-center"><b>NUESTRAS CAMPAÑAS</b></h6>
                                        <hr>
                                        <h6 class="weight-600" style="text-align:center">Sección en mantenimiento.</h6>
                                        <hr>
                                        <center><a href="javascript:$('.sectionSlider').backToSlide()" class="btn no-smooth-scroll"><i class="material-icons">fast_rewind</i></a></center>
                                    </div>
                                    <div class="col s12 m9 l8 offset-m1 animatedBox animatedBox-right">
                                        <div class="fadein video-container">
                                            <img src="img/campañas/campaña-1.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/campañas/campaña-2.jpg" class="responsive-img z-depth-3" />
                                            <img src="img/campañas/campaña-3.jpg" class="responsive-img z-depth-3" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>  
            </section>
        </div>
    </div>
    <!--Fin Clinica = Work -->
     <div class="about-tabs tabs-wrapper hide-on-med-and-down">
                <div class="row white">
                    <div class="container">
                        <ul class="sticky-tabs tabs">
                            <li class="tab col s4">
                                <a href="#photography" onclick="$('.photographySlider').moveToSlide(1)">EXÁMENES</a>
                            </li>
                            <li class="tab col s4">
                                <a href="#filmmaking" onclick="$('.filmmakingSlider').moveToSlide(1)">CONSULTAS</a>
                            </li>
                            <li class="tab col s4">
                                <a href="#web" onclick="$('.webSlider').moveToSlide(1)">CIRUGÍAS</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="separator separator6 hide-on-med-and-down"></div>
            <script>
                createParallax('separator6', '.separator6', 0.01);
            </script>        
    <div id="what" class="section-link">
        <!-- Seccion examenes = photography -->
        <div id="photography" class="inner-section-link">
            <section>
                <div class="sectionSlider photographySlider">
                    <ul>
                        <li>
                            <div class="container">
                                <div class="row">
                                    
                                    <div class="col s8 m6 l5 offset-m3 offset-s2 hide-on-large-only">
                                        <img src="img/examenes/examenes.jpg" class="circle responsive-img">
                                    </div>

                                    <div class="col l7 m12 s8 offs2et-s2 animatedBox animatedBox-left animatedBox-list">
                                        <h2 class="weight-100">EXÁMENES</h2>
                                        <!-- 1 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="" class="weight-800 grey-text darken-3">1. Refraccón (Medida de la Vista)</a>
                                                <a href="#modalnuevo" class="modal-trigger btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a>
                                                                                                
                                            </div>
                                            <div class="modal" id="modalnuevo">
                                                <div class="modal-content">
                                                        <h4 class="center"><br/>REFRACCIÓN (MEDIDA DE LA VISTA)</h4>
                                                        <p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est fuga vitae commodi earum nobis, sequi expedita velit voluptas sunt animi quibusdam voluptate dolor quam aliquid necessitatibus temporibus, perferendis nisi aperiam!</p>
                                                </div>
                                                </div>
                                        </div>
                                        <!--Fin 1 Item -->
                                        <br>
                                        <!-- 2 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(3)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">2. Refracción con Ciclopejía</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(3)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!-- Fin 2 Item -->
                                        <br>
                                        <!-- 3 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(4)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">3. Adaptación de lentes de contacto</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(4)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!-- Fin 3 Item -->
                                        <br>
                                        <!-- 4 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(5)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">4. Baja Visión</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(5)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!-- Fin 4 Item -->                                       
                                    </div>

                                    

                                    <div class="col l5 hide-on-med-and-down animatedBox animatedBox-right">
                                        <img src="img/examenes/examenes.jpg" class="circle responsive-img">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="container">
                                <div class="row">
                                    
                                    <div class="col s8 m6 l5 offset-m3 offset-s2 hide-on-large-only">
                                        <img src="img/examenes/examenes-1.jpg" class="circle responsive-img">
                                    </div>

                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-left animatedBox-list">
                                        <h2 class="weight-100">EXÁMENES</h2>
                                        <!-- 1 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(2)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">5. Campimetría</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(2)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!--Fin 1 Item -->
                                        <br>
                                        <!-- 2 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(3)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">6. Paquimetria</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(3)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!-- Fin 2 Item -->
                                        <br>
                                        <!-- 3 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(4)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">7. Retinografía</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(4)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!-- Fin 3 Item -->
                                        <br>
                                        <!-- 4 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(5)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">8. Retinofluoresceinografía</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(5)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!-- Fin 4 Item -->                                       
                                    </div>
                                    <div class="col l5 hide-on-med-and-down animatedBox animatedBox-right">
                                        <img src="img/examenes/examenes-1.jpg" class="circle responsive-img">
                                    </div>
                                </div>
                            </div>
                        </li>
                                                <li>
                            <div class="container">
                                <div class="row">
                                    
                                    <div class="col s8 m6 l5 offset-m3 offset-s2 hide-on-large-only">
                                        <img src="img/examenes/examenes-2.jpg" class="circle responsive-img">
                                    </div>

                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-left animatedBox-list">
                                        <h2 class="weight-100">EXÁMENES</h2>
                                        <!-- 1 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(2)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">9. Gonioscopía</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(2)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!--Fin 1 Item -->
                                        <br>
                                        <!-- 2 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(3)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">10. Ecografía Ocular</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(3)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!-- Fin 2 Item -->
                                        <br>
                                        <!-- 3 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(4)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">11. Biometría Ocular</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(4)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!-- Fin 3 Item -->
                                        <br>
                                        <!-- 4 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(5)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">12. Oftalmoscopia Indirecta</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(5)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!-- Fin 4 Item -->                                       
                                    </div>
                                    <div class="col l5 hide-on-med-and-down animatedBox animatedBox-right">
                                        <img src="img/examenes/examenes-2.jpg" class="circle responsive-img">
                                    </div>
                                </div>
                            </div>
                        </li>
                                                <li>
                            <div class="container">
                                <div class="row">
                                    
                                    <div class="col s8 m6 l5 offset-m3 offset-s2 hide-on-large-only">
                                        <img src="img/examenes/examenes-3.jpg" class="circle responsive-img">
                                    </div>

                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-left animatedBox-list">
                                        <h2 class="weight-100">EXÁMENES</h2>
                                        <!-- 1 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(2)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">13. Tomografía Láser para Glaucoma</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(2)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!--Fin 1 Item -->
                                        <br>
                                        <!-- 2 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(3)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">14. Tomografía de coherencia óptica</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(3)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!-- Fin 2 Item -->
                                        <br>
                                        <!-- 3 Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.photographySlider').moveToSlide(4)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">15. Test de Schirmer</a>
                                                <span><a href="javascript:$('.photographySlider').moveToSlide(4)" class="btn-flat no-smooth-scroll right btn-inline orange-text">Leer Más</a></span>
                                            </div>
                                        </div>
                                        <!-- Fin 3 Item -->
                                        <br>                                     
                                    </div>
                                    <div class="col l5 hide-on-med-and-down animatedBox animatedBox-right">
                                        <img src="img/examenes/examenes-3.jpg" class="circle responsive-img">
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            
                <div class="nextLink hide-on-med-and-down"><a href="#filmmaking" class="btn-floating btn-large waves-effect waves-dark grey darken-3"><i class="material-icons">open_with</i></a>
                </div>
            </section>
            <!-- End of Section
          –––––––––––––––––––––––––––––––––––––––––––––––––– -->

            <!-- Separator
          –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <div class="separator2"></div>
            <script>
                createParallax('separator2', '.separator2');
            </script>
        </div>
        <!-- Filmmaking Section
          –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <div id="filmmaking" class="inner-section-link">
            <section>
                <div class="sectionSlider filmmakingSlider">
                    <ul>
                        <li>
                            <div class="container">
                                <div class="row">
                                    <div class="col s8 m6 l5 offset-m3 offset-s2 animatedBox animatedBox-left">
                                        <img src="img/consultas/consulta-1.jpg" class="circle responsive-img">
                                    </div>
                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-right animatedBox-list">
                                        <h2 class="weight-100">CONSULTAS</h2>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(2)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">1. Examen de Agudeza Visual</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(2)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <br>
                                        <!-- End of Item -->
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(3)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">2. Presión Intraocular</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(3)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <br>
                                        <!-- End of Item -->
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(4)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">3. Examen de Fondo de Ojo</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(4)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <br>
                                        <!-- End of Item -->
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(5)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">4. Examen de Segmento Anterior</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(5)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="container">
                                <div class="row">
                                    <div class="col s8 m6 l5 offset-m3 offset-s2 animatedBox animatedBox-left">
                                        <img src="img/consultas/consulta-2.jpg" class="circle responsive-img">
                                    </div>
                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-right animatedBox-list">
                                        <h2 class="weight-100">CONSULTAS</h2>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(2)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">5. Motilidad Ocular</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(2)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <br>
                                        <!-- End of Item -->
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(3)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">6. Medida de Vista con Sistema Digital</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(3)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <br>
                                        <!-- End of Item -->
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(4)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">7. Descarte de Enfermedades Oculares</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(4)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <br>
                                        <!-- End of Item -->
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(5)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">8. Segmento Anterior</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(5)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="container">
                                <div class="row">
                                    <div class="col s8 m6 l5 offset-m3 offset-s2 animatedBox animatedBox-left">
                                        <img src="img/consultas/consulta-3.jpg" class="circle responsive-img">
                                    </div>
                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-right animatedBox-list">
                                        <h2 class="weight-100">CONSULTAS</h2>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(2)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">9. Retina y Vitreo</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(2)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <br>
                                        <!-- End of Item -->
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(3)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">10. Cornea</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(3)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <br>
                                        <!-- End of Item -->
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(4)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">11. Glaucoma</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(4)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <br>
                                        <!-- End of Item -->
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(5)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">12. Oculoplástica</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(5)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                    </div>
                                </div>
                            </div>
                        </li>
                                                <li>
                            <div class="container">
                                <div class="row">
                                    <div class="col s8 m6 l5 offset-m3 offset-s2 animatedBox animatedBox-left">
                                        <img src="img/consultas/consulta-4.jpg" class="circle responsive-img">
                                    </div>
                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-right animatedBox-list">
                                        <h2 class="weight-100">CONSULTAS</h2>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(2)" class="weight-800 grey-text darken-3 modal-trigger hover no-smooth-scroll">13. Uveítis</a>
                                                <a href="javascript:$('.filmmakingSlider').moveToSlide(2)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="nextLink hide-on-med-and-down"><a href="#web" class="btn-floating btn-large waves-effect waves-dark grey  darken-3"><i class="material-icons">open_with</i></a>
                </div>
            </section>
            <!-- End of Section
          –––––––––––––––––––––––––––––––––––––––––––––––––– -->

            <!-- Separator
          –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <div class="separator separator3"></div>
            <script>
                createParallax('separator3', '.separator3');
            </script>
        </div>
        <!-- Web Development Section
          –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <div id="web" class="inner-section-link">
            <section>
                <div class="sectionSlider webSlider">
                    <ul>
                        <li>
                            <div class="container">
                                <div class="row">
                                    <div class="col s8 m6 offset-m3 offset-s2 hide-on-large-only">
                                        <img src="img/cirugias/cirugia-1.jpg" class="circle responsive-img">
                                    </div>
                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-left animatedBox-list">
                                        <h2 class="weight-100">RETINA y VÍTREO</h2>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(2)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">1. Vitrectomia</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(2)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                        <br>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(3)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">2. Cerclaje</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(3)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a></span>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                        <br>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(4)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">3. Lensectomía</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(4)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                        <br>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(5)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">4. Extracción Cuerpo Extraño Intraocular</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(5)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                    </div>
                                    <div class="col l5 hide-on-med-and-down animatedBox animatedBox-right">
                                        <img src="img/cirugias/cirugia-1.jpg" class="circle responsive-img">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="container">
                                <div class="row">
                                    <div class="col s8 m6 offset-m3 offset-s2 hide-on-large-only">
                                        <img src="img/cirugias/cirugia-2.jpg" class="circle responsive-img">
                                    </div>
                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-left animatedBox-list">
                                        <h2 class="weight-100">PLÁSTICA OCULAR</h2>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(2)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">1. Crioterapia y Ciclocrioterapia</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(2)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                        <br>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(3)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">2. Aplicación de BOTOX Zona Oculofacial</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(3)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a></span>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                        <br>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(4)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">3. Blefaroplastia</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(4)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                        <br>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(5)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">4. Dacriocistorinostomia</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(5)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                    </div>
                                    <div class="col l5 hide-on-med-and-down animatedBox animatedBox-right">
                                        <img src="img/cirugias/cirugia-2.jpg" class="circle responsive-img">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="container">
                                <div class="row">
                                    <div class="col s8 m6 offset-m3 offset-s2 hide-on-large-only">
                                        <img src="img/cirugias/cirugia-3.jpg" class="circle responsive-img">
                                    </div>
                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-left animatedBox-list">
                                        <h2 class="weight-100">CATARATA</h2>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(2)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">1. Facoemulsificación</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(2)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                        <br>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(3)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">2. MININUC</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(3)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a></span>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                    </div>
                                    <div class="col l5 hide-on-med-and-down animatedBox animatedBox-right">
                                        <img src="img/cirugias/cirugia-3.jpg" class="circle responsive-img">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="container">
                                <div class="row">
                                    <div class="col s8 m6 offset-m3 offset-s2 hide-on-large-only">
                                        <img src="img/cirugias/cirugia-4.jpg" class="circle responsive-img">
                                    </div>
                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-left animatedBox-list">
                                        <h2 class="weight-100">SEGMENTO ANTERIOR</h2>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(2)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">1. Pterigion</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(2)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                        <br>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(3)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">2. Lasik (Refractiva)</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(3)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a></span>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                    </div>
                                    <div class="col l5 hide-on-med-and-down animatedBox animatedBox-right">
                                        <img src="img/cirugias/cirugia-4.jpg" class="circle responsive-img">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="container">
                                <div class="row">
                                    <div class="col s8 m6 offset-m3 offset-s2 hide-on-large-only">
                                        <img src="img/cirugias/cirugia-5.jpg" class="circle responsive-img">
                                    </div>
                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-left animatedBox-list">
                                        <h2 class="weight-100">GLAUCOMA</h2>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(2)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">1. Trabeculectomía</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(2)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                        <br>
                                        <!-- Item -->
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="javascript:$('.webSlider').moveToSlide(3)" class="weight-800 grey-text darken-3 hover no-smooth-scroll">2. Implantes Valvulares</a>
                                                <a href="javascript:$('.webSlider').moveToSlide(3)" class="btn-flat no-smooth-scroll right btn-inline orange-text">LEER MÁS</a></span>
                                            </div>
                                        </div>
                                        <!-- End of Item -->
                                    </div>
                                    <div class="col l5 hide-on-med-and-down animatedBox animatedBox-right">
                                        <img src="img/cirugias/cirugia-5.jpg" class="circle responsive-img">
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="nextLink hide-on-med-and-down"><a href="#software" class="btn-floating btn-large waves-effect waves-dark grey darken-3"><i class="material-icons">open_with</i></a></div>
            </section>
            <!-- End of Section
          –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <div class="separator separator7"></div>
            <script>
                createParallax('separator7', '.separator7');
            </script>
        </div>
    </div>
    <div id="who" class="section-link">
        <!-- Filmmaking Section
          –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <div class="inner-section-link">
            <section>
                <div class="sectionSlider aboutSlider">
                    <ul>

                        <?php  
                                 $me=new Doctor();
                                    $r=$me->listar_medicos();
                                    for($i=0;$i<count($r);$i++)
                                    {
                        ?>                                        
                                
                        <li>
                            <div class="container">
                                <div class="row">
                                    <div class="col s8 m6 l5 offset-m3 offset-s2 hide-on-large-only">
                                        
                                    <?php 

                                    //Imagenes de Staff Médico
                                    
                                    $ruta = "img/staff/" . $r[$i]['foto'];
                                    echo "<img src='$ruta' class='circle responsive-img' />";
                                    echo "<br>";
                                    ?>
                                    </div>
                                    <div class="col l7 m12 s8 offset-s2 animatedBox animatedBox-left animatedBox-list">
                                       
                                       <?php 
                                        echo "<h3 align='center' class='weight-100' >".$r[$i]['Nombres']."</h3>";
                                        echo "<p align='center' class='weight-800 grey-text darken-3'> CMP: ".$r[$i]['CMP']."</p>";
                                        echo "<p align='center' class='weight-800 grey-text darken-3'> Cargo: ".$r[$i]['Cargo']."</p>";
                                        echo "<p align='center' class='weight-800 grey-text darken-3'> Especialidad: ".$r[$i]['Especialidad']."</p>"; 

                                       ?>
                                    </div>
                                    <div class="col l5 hide-on-med-and-down animatedBox animatedBox-right">
                                       <?php 

                                    //Imagenes de Staff Médico
                                    
                                    $ruta = "img/staff/" . $r[$i]['foto'];
                                    echo "<img src='$ruta' class='circle responsive-img' />";
     
                                    ?>

                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php 
                           } 
                        ?>
                        
                    </ul>
                </div>
                <div class="nextLink hide-on-med-and-down"><a href="#gallery" class="btn-floating btn-large waves-effect waves-dark grey  darken-3"><i class="material-icons">open_with</i></a>
                </div>
            </section>
            <!-- End of Section
          –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <!-- Separator
          –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <div class="separator separator1"></div>
            <script>
                createParallax('separator1', '.separator1');
            </script>
        </div>
        <div id="gallery" class="section-link">
            <section>
                <div class="container">
                    <h2 class="weight-100">CONOCE NUESTRAS MARCAS</h2>
                    <div class="row">
                        <div class="col s12">
                        </div>
                        <div id="all" class="col s12">
                            <div class="row galleryContainer">
                            </div>
                        </div>
                        <div id="product" class="col s12"></div>
                        <div id="portrait" class="col s12"></div>
                        <div id="nature" class="col s12"></div>
                        <div id="architecture" class="col s12"></div>
                    </div>
                </div>
            </section>
            <script>
                function createGallery() {
                    for (i = 0; i <= 13; i++) {
                        if (i < 10) {
                            $('.galleryContainer').append($('<div class="col l4 s6"><img src="img/gallery0' + i + '.jpg" class="materialboxed responsive-img"/></div>'));
                        } else {
                            $('.galleryContainer').append($('<div class="col l4 s6"><img src="img/gallery' + i + '.jpg" class="materialboxed responsive-img"/></div>'));
                        }
                    }
                }
                createGallery();
            </script>
        </div>
    </div>
    <!-- Footer -->
    <!-- Footer Section
      –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <div id="how" class="">
        <footer class="grey darken-1">
            <div class="container">
                <div class="row">
                    <h2 class="col m8 s10 offset-s1 white-text text-lighten-1 weight-100"><b>CONTÁCTANOS</b></h2>
                </div>
                <div class="row">
                    <span class="col m6 s10 offset-s1 non-fluid white-text  weight-600">
                      <form action="index.php" id="myForm" method="POST">
                             <div class="input-field">
                        <i class="material-icons prefix">person_pin</i>
                        <input id="icon_prefix" type="text" name="nom" id="nom" class="validate">
                        <label for="icon_prefix">Nombre</label>
                        </div>
                        <div class="input-field">
                        <i class="material-icons prefix">phone</i>
                        <input id="icon_phone" type="text" name="telefono" class="validate">
                        <label for="icon_phone">Telefono/Celular</label>
                        </div>
                         <div class="input-field">
                        <i class="material-icons prefix">email</i>
                        <input id="icon_email" type="text" name="email" class="validate">
                        <label for="icon_email">E-mail</label>
                        </div>
                        <div class="input-field">
                        <i class="material-icons prefix">visibility</i>
                        <input id="icon_visibility" type="text" name="asunto" class="validate">
                        <label for="icon_visibility">Asunto</label>
                        </div>
                        <div class="input-field">
                        <i class="material-icons prefix">chat</i>
                        <textarea id="textarea1" name="consulta" class="materialize-textarea"></textarea>
                        <label for="textarea1">Mensaje</label>
                        </div>
                        <center><button type="submit" name="enviar"  class="waves-effect waves-light btn" style="margin-right: 20px; margin-top: 15px; margin-bottom: -12px;"><i class="material-icons right">play_arrow</i> Enviar</button></center>  
                      </form>

                      <?php 

                    require 'correo/PHPMailerAutoload.php';
                    require 'correo/class.phpmailer.php';
                    require 'correo/class.smtp.php';


                    if(isset($_POST['enviar'])){

                    $nombre=$_POST['nom'];
                    $telefono=$_POST['telefono'];
                    $a=$_POST['email'];
                    $asunto=$_POST['asunto'];
                    $consulta=$_POST['consulta'];

                    $mail = new PHPMailer;
                    $mail->isSMTP();                                      
                    $mail->Host = 'mail.fundacionvista.com.pe';  
                    $mail->SMTPAuth = true;
                                                   
                    $mail->Username = 'sistemas@fundacionvista.com.pe';
                    $mail->Password = 'SISTEMAS2015';
                    $mail->SMTPSecure = 'smtp';                            // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = 25;                                    // TCP port to connect to

                    $mail->setFrom($a,$nombre);


                    $mail->addAddress('informes@fundacionvista.com.pe');

                    $mail->isHTML(true);                                  

                    $mail->Subject =$asunto."   (".$a.")";

                    $resptelefono=$consulta."<br><br>"."Telefono/Celular: ".$telefono;

                    $mail->Body=$resptelefono;
                    $mail->AltBody = $a;
                    $mail->MsgHTML($resptelefono,$a);

                    if(!$mail->send()) {
                        
                        echo "<script>
                     Materialize.toast('Error en el envío', 4000, 'rounded');
                    </script>";

                    } else {
                    echo "<script>
                     Materialize.toast('Mensaje Enviado Correctamente. Inmediatamente nos comunicaremos con usted.', 41000, 'rounded');
                    </script>";
                    }

                    }
                ?>
   
                    </span>
                    <div class="col m4 offset-m2 s12">
                        <ul class="white-text">
                            <li><i class="small material-icons left">location_on</i><b>Jr. Caracas N° 2410 - Jesús María</b></li> <br>
                            <li><i class="small material-icons left">email</i><b>informes@fundacionvista.com</b></li> <br>
                            <li><i class="small material-icons left">call</i><b>(01) 641-3217</b></li>
                        </ul>
                        <br>
                        <br>
                        <h5 class="weight-100 white-text center" style="margin:0"><b>FUNDACI&Oacute;N VISTA</b></h5>
                        <h6 class="weight-400 white-text center" style="margin:0">VISIÓN - GLAUCOMA - RETINA</h6>
                        <br>
                        <span class="col s12 center white-text weight-100">© 2015</span>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    <div class="row">
                        <div class="center col s12">
                            <h6>Powered by: FUNDACIÓN VISTA | © 2015 Fundación Vista Inc. All Rights Reserved.</h6>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- End of Section -->
    <script>
        //Parallax
        if (touch) $('.about-tabs').hide();
        $('.img-holder').imageScroll({
            imageAttribute: (touch === true) ? 'image-mobile' : 'image',
            touch: touch,
            holderMinHeight: 500
        });
        $(document).ready(function(){
            $('.modal-trigger').leanModal();
        });
    </script>
    </div>    
</body>
    <a href='https://www.facebook.com/pages/Fundaci%C3%B3n-Vista-Cirug%C3%ADa-Oftalmol%C3%B3gica-Avanzada/339933439428377?fref=ts' title='Sígueme en Facebook' target='_blank'><img border='0'src='http://1.bp.blogspot.com/-7VSvlkal0os/UFORwVAFAZI/AAAAAAAAHkg/nhegYirxh5g/s1600/boton+facebook_opt.png'style='display:scroll;position:fixed;bottom:350px;left:10px' alt=""/></a> 
    <a href='https://twitter.com/fundacionvista' title='Sígueme en Twitter' target='_blank'><img border='0' src='http://2.bp.blogspot.com/-r2kTvDo3bfw/Toc-MKnFfxI/AAAAAAAAA3A/3f86LWBmR38/s1600/boton%252520twitter.png' style='display:scroll;position:fixed;bottom:150px;left:10px;' alt=""/></a>
    <a href='magazine/index.html'title='Catálogo Virtual' target='_blank'><img src="img/open161.png" style='display:scroll;position:fixed;bottom:70px;left:5px;' alt=""/></a>
</html>